"""
This is a unit test file for calc.py
"""

import unittest
from Courses.Workshops.DuoPro.calculator.final.calc import add, subtract, multiply

class TestCalc(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(1, 2), 5)
        self.assertEqual(add(-1, 1), 0)
        self.assertEqual(add(-1, -1), -2)
                    
    def test_subtract(self):
        self.assertEqual(subtract(1, 1), 0)
        self.assertEqual(subtract(-1, -1), 0)
        self.assertEqual(subtract(-1, 1), -2)

# Add the script main entry
if __name__ == '__main__':
    unittest.main()

