"""
This module is used to create calculation functions such as addition, subtraction, multiplication, and division.
This module can be executed as a command line script using Click where the numbers are passed in as arguments and echo the results using Click
"""

import click

def add(a, b):
    """
    This function adds two numbers together.
    """
    return a + b

def subtract(a, b):
    """
    This function subtracts two numbers.
    """
    return a - b

def multiply(a, b):
    """
    This function multiplies two numbers.
    """
    return a * b

# Add a command group

@click.group()
def cli():
    pass

# add the click command named 'addnumbers' that accepts two integer arguments, and invoke add function
# The output will be echo to the console
# The command will use the short name 'add' instead of 'addnumbers'

@cli.command('add', short_help='Add two numbers')
@click.argument('a', type=int)
@click.argument('b', type=int)  
def addnumbers(a, b):
    # result will be styled in green before been echoed to the console
    click.echo(click.style(f'{a} + {b} = {add(a, b)}', fg='green'))

# Add the click command named 'subtractnumbers' that accepts two integer arguments, and invoke subtract function
    
@cli.command('subtract', short_help='Subtract two numbers')
@click.argument('a', type=int)
@click.argument('b', type=int)
def subtractnumbers(a, b):
    click.echo(click.style(f'{a} - {b} = {subtract(a, b)}', fg='green'))
    

# Add the class main to be the cli() function
if __name__ == '__main__':
    cli()
