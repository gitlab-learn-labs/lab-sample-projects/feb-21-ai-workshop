# Calculator

[[_TOC_]]

## Overview
This tutorial walkthrough coding with code suggestion

## Pre-requisites
- [VSCode Extension](../../AI/Optional_Enable_Code_Suggestions_In_VSC.md) or [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)
- [Installed Python](https://code.visualstudio.com/docs/python/python-tutorial#_install-a-python-interpreter)
- Execution of Bash scripts, for windows user you can install [Git Bash](https://www.educative.io/answers/how-to-install-git-bash-in-windows)

<!--
**Covered use cases**
- [Build boiler plate](#build-boiler-plate)
- [Build test cases](#build-test-cases)
-->

## Build boiler plate code

1. Lets start by creating a new file 'calc.py' in the practice folder

2. describe what the class will do, and code suggestion will create the add and subtract function

```python
"""
This module is used to create calculation functions such as addition and subtraction
"""
```

3. Let's use the Click package to allow the module to be executed as a command line script

```python
"""
This module is used to create calculation functions such as addition and subtraction
This module can be executed as a command line script using Click, and passing in two numbers as arguments and echo the results
"""
```
4. See code suggestion import in the click module or manually type

```python
import click
```

4. Add the click group 

```python
# add the click group
```

5. Add the click command 'addnumber'

```python
# add the click command named 'addnumbers' that accepts two integer arguments, and invoke add function
# The output will be echo to the console
# The commend will use the short name 'add' instead of 'addnumbers'
```

6. script main entry is the cli() function

```python
# Add the script main entry which points to the cli() function
```

7. Execute the script

```bash
python3 calc.py add 1 2
```

8. Tweak the output to be green from click

Enter the below comment above the echo statement, and partially delete the echo statement
```python
# the result will be styled in green before been echoed to the console
```

9. Add the click command 'subtractnumbers'

```python
# add the click command subtractnumbers to subtract the two numbers passed in as integer arguments
# the command will use the short name 'subtract' instead of 'subtractnumbers'
```

## Build test cases

1. Create a new file called 'test_calc.py' in the practice folder

2. Lets add a header comment to descibe this file

```python
"""
This is a unit test file for calc.py
Write good and bad test cases for function add and subtract
"""
```

2. Code suggestion should prompt for the import and other suggestions

```python
import unitest
from calc import add, subtract
```

3. script main entry is the cli() function

```python
# Add the script main entry
```

4. Execute the script

```bash
python3 test_calc.py
```

5. Try to alter the use case, to see failure
