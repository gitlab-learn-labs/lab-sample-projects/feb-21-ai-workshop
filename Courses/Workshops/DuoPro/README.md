# Duo Pro Hands-on

This tutorial illustrates hands-on examples using Code Suggestions and Duo Chat

1. [Calculator](calculator/README.md)  
2. [Loops](loops/README.md)
