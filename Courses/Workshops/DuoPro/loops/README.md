# Loops

[[_TOC_]]

## Overview
This tutorial covers several use case with Duo Chat, based on the example bash script [loops.sh](loops.sh)

To execute the loops bash script

```bash
chmod a+x loops.sh
bash loops.sh
```

## Pre-requisites
- [VSCode Extension](../../AI/Optional_Enable_Code_Suggestions_In_VSC.md) or [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)
- [Installed Python](https://code.visualstudio.com/docs/python/python-tutorial#_install-a-python-interpreter)
- Execution of Bash scripts, for windows user you can install [Git Bash](https://www.educative.io/answers/how-to-install-git-bash-in-windows)

## Document code

```duo
document code
```

### Example response

Here is one way to document the provided code sample in markdown format:

```bash
#!/usr/bin/env bash

# Loop through the array of fruits
for fruit in apple banana pear; do

  # Print a message saying we love eating each fruit
  echo "I love eating healthy snacks like this $fruit";
  
done
```

This loops through the array of fruits - apple, banana, pear - and prints a message for each one saying "I love eating healthy snacks like this" followed by the fruit name.

The key aspects that could be documented:

- Shebang line specifying to use bash interpreter
- For loop syntax
- Fruit array defined
- Echo command to print message
- $fruit variable substitution
- done keyword to end loop

## Explain this code

```duo
/explain
```

### Example response

This is a simple for loop in Bash shell script that iterates over a list of fruits. Here is an explanation of what it does:
```bash
#!/usr/bin/env bash
```
This first line calls the bash interpreter to execute the script.

```bash
for fruit in apple banana pear;
```
This starts a for loop that will iterate over the list of fruits - apple, banana and pear. On each iteration, the variable fruit will be set to the next fruit in the list.

```bash
do echo "I love eating healthy snacks like this $fruit";
```
This is the body of the for loop. It will be executed on every iteration. It prints out the string "I love eating healthy snacks like this" along with the value of the $fruit variable, which will print the current fruit.
```bash
done
```
This done keyword signifies the end of the for loop body.

So in summary, this loop will:

Iterate over apple, banana and pear
Print "I love eating healthy snacks like this apple"
Print "I love eating healthy snacks like this banana"
Print "I love eating healthy snacks like this pear"
Printing that string for each of the fruits in the list.

## Simplify the source code

```duo
simplify code
```

### Example response

Here is one way to simplify the provided code snippet:
```bash
#!/usr/bin/env bash
fruits=(apple banana pear) for fruit in "${fruits[@]}"; do echo "I love eating healthy snacks like this $fruit" done
```

In this simplified version:

An array is used to define the list of fruits rather than repeating the "fruit in" statement
The fruits array is looped through directly rather than specifying each fruit individually
The echo statement is kept the same otherwise
This removes some of the duplication in defining each fruit separately. 

## Translating the loops bash script to python

```duo
/refactor to python
```

- Execute the python script

```bash
python3 translated/loops.py
```


## Example response

Here is the refactored Python code:

```python
fruits = ["apple", "banana", "pear"]
for fruit in fruits:
    print(f"I love eating healthy snacks like this {fruit}")
```

I refactored the Shell script to Python by:

Defining a list fruits to store the fruit values instead of specifying them in the for loop
Using a Python for loop to iterate over the fruits list
Printing the string with the fruit interpolated using an f-string instead of echo
Removed the semicolons and other Shell syntax not needed in Python


## Create unit test

```duo
/tests
```

## Example response

Here are some tests I would write for the selected code:

```bash
#!/bin/bash

# Test that the loop iterates over each fruit
fruits=("apple" "banana" "pear")
for i in "${fruits[@]}"; do
  output=$(bash loops.sh | grep "$i")
  [ ! -z "$output" ]
done

# Test that the output contains the expected string
expected="I love eating healthy snacks like this"
output=$(bash loops.sh)
[[ "$output" == *"$expected"* ]]

# Test invalid input
output=$(bash loops.sh 2>&1 | grep "not found")
[ -z "$output" ]

```

This first test iterates through the expected fruits and verifies that the loop outputs contain each one. The second test checks that the output contains the expected string about healthy snacks. The third test passes invalid input to verify robustness.