You can also find a blog about other supported [Web IDE's here](https://about.gitlab.com/blog/2023/06/01/extending-code-suggestions/)

# Step 1: Connecting VSC
1. Make sure you have VSC installed on your device, if you don't you can download it [here](https://code.visualstudio.com/Download)

2. Next navigate to your VSC instance and on the left hand navigation menu click the **Extensions** button. Once there we want to put 'GitLab Workflow' in the search bar and install.

3. Once installed and enabled click the settings icon then click **Extension settings**. On the resulting page at the top of the settings click the checkbox next to **Enable code completion**


# Step 1.1: Workaround for non-https web proxy
Code suggestions requires connectivity to GitLab AI Gateway (https://cloud.gitlab.com), and if a web proxy is required, its mandatory to for a HTTPS proxy.

This HTTPS proxy is used by the GitLab VSC extension to route the traffic.

In the event that your organization only supports a HTTP proxy, the workaround below needs to be performed.

1. Set the environment variable 'HTTPS_PROXY' in your shell

```ps
$env:HTTPS_PROXY="http://proxyint.intra.bca.co.id:8080"
```

2. Startup your VSC via the shell for the environment setting to take effect

```ps
code .
```

![HTTPS Proxy workaround](https_proxy.png)

<!--

To pass the proxy settings to extension

1. Open Visual Studio Code

2. Navigate to File > Preferences > Settings

3. Select Application > Proxy in the sidebar

4. In the available fields, insert the proxy URL and credentials as: http://user:pass@my.proxy.address:8080

Note: To ensure proxy are set correctly, please ensure that the HTTP proxy settings are set to 'override'. To do, complete the following steps: 

1. Open Visual Studio Code

2. Go to Settings > Application > Proxy

3. In the drop down list next to ‘http: Proxy Support’ select ‘override’

-->


# Step 2: Authenticating With GitLab
  **OAuth Method (recommended)**
1. Still on VSC we want to add our GitLab account. Even if you have done this before make sure you reset access as some settings have changed:
    - In macOS, press Shift + Command + P.
    - In Windows, press Shift + Control + P.

2. We then want to input **_'GitLab: Authenticate with GitLab.com'_**, click the quick action with the same name, and follow the alert links to open the auth portal for gitlab.com.

3. Once authed, there should be an alert that will bring you back to VSC. At this point you should be ready to go and not need to attempt the personal access token method.

  **Personal Access Token Method**
1. Navigate to your gitlab.com view, then click your accounts icon in the top right. On the resulting dropdown click **Preferences**.
  
2. We then want to use the left hand navigation menu to click **Access Tokens**. Once on the **_Personal Access Tokens_** page we will want to name our token VSC, give it an expiration date (we suggest at least 30 days), then select all scopes.

3. With all of the settings set we will click **Create personal access token** and then copy the token locally as we will need to use it later in the workshop.

4. We then want to add our GitLab account to VSC. Even if you have done this before make sure you reset the token as some settings have changed:
    - In macOS, press Shift + Command + P.
    - In Windows, press Shift + Control + P.

5. Next type **'GitLab: Add Account to VS Code'** and click the quick action with the same name.

6. We then want to input 'https://gitlab.com/', then provide your personal access token that we got earlier.

You should now start seeing code suggestions pop up in VSC, same as you do in the Web IDE. If you want to use other editors such as JetBrains IDEs, Neovim, and more check out our docs [here](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#supported-editor-extensions).

# Step 3: Clone your Workshop Project
1. Now we need to make sure that we have our project ready to go on VSC. Navigate to the home page of our **_workshop-project_** and click the **clone** button. We then want to copy the whole URL in the **_Clone with HTTPS_** option.

# Troubleshooting

## Disable streaming
By default, code generation streams AI-generated code. When streaming is enabled, the suggested code is generated and sent to your editor incrementally rather than waiting for the full code snippet to be complete before sending the suggestions.

This allows for a more interactive and responsive experience.
If you prefer to only see code generation results printed in their entirety, you can turn off streaming.

Disabling streaming means that code generation requests may be perceived as taking longer to resolve. To disable streaming:

1. Edit your settings.json file.
2. Add this line, or edit it if it already exists, to disable code suggestions streaming:
```json
"gitlab.featureFlags.streamCodeGenerations": false,
```
3. Save your changes.

[MR Link](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/docs/user/troubleshooting.md#disable-streaming)

## Other troubleshooting
See [Troubleshooting code suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/troubleshooting.html)


# References
Supported IDE extensions [[1]]

[1]:https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html
